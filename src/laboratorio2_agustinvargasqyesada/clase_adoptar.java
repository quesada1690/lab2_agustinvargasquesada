package laboratorio2_agustinvargasqyesada;

import java.util.Scanner;

/**
 *
 * @author varga
 */
public class clase_adoptar {

    public void adoptar_mascota(String[] args) {
        Scanner enteros = new Scanner(System.in);
        Scanner caracter = new Scanner(System.in);

        System.out.println("  ");
        System.out.println("  ");
        System.out.println("  ");

        for (int g = 0; g <= objeto_mascota.listaMascotas.size() - 1; g++) {
            objeto_mascota datos_mascota = objeto_mascota.listaMascotas.get(g);
            int id = datos_mascota.getId_mascota();
            String nom_mascota = datos_mascota.getNombre_mascota();
            String tipo_mascota = datos_mascota.getTipo();
            String estado_actual = datos_mascota.getEstado();
            System.out.println("Id mascota; " + id + " Nombre: " + nom_mascota + " Tipo: " + tipo_mascota + " Estado: " + datos_mascota.getEstado());
        }
        System.out.println("  ");
        
        
        //codigo para adoptar una mascota
        System.out.println("Digite el numero de cedula:");
        int cedula_buscar = enteros.nextInt();
        boolean existe = false;

        //recorre la lista de las personas para adopatar
        for (int j = 0; j <= objeto_persona.listaPersonas.size() - 1; j++) {

            objeto_persona datos_personas = objeto_persona.listaPersonas.get(j);

            //verifica si la perona existe 
            if (cedula_buscar == datos_personas.getCedula()) {

                System.out.println("Digite la ID de la mascota :");
                int id_buscar = caracter.nextInt();
                String estado_mascota = "Disponible";

                //recorre la lista de las mascotas 
                for (int i = 0; i <= objeto_mascota.listaMascotas.size() - 1; i++) {
                    objeto_mascota datos_mascota = objeto_mascota.listaMascotas.get(i);

                    //verifica que la mascota exista y se pueda adoptar
                    if ((id_buscar == datos_mascota.getId_mascota()) && (estado_mascota.equals(datos_mascota.getEstado()))) {
                        System.out.println("Usted a adoptado a la mascota");
                        datos_mascota.setEstado("Adoptada");
                        objeto_registro.reportes.add(new objeto_registro(cedula_buscar, id_buscar));
                    } else {
                        System.out.println("La mascota ya a sido adoptada");
                    }

                }
                existe = true;

            }
        }
        if (existe == false) {
            System.out.println("La persona no existe, verifique los datos");
        }

        for (int t = 0; t <= objeto_registro.reportes.size() - 1; t++) {
            objeto_registro reportes = objeto_registro.reportes.get(t);
            int dueño = reportes.getDueño();
            int mascota_nueva = reportes.getMascota_adoptada();
            System.out.println("Id mascota adopotada: " + mascota_nueva + " Cedula persona que adopta: " + dueño);
        }

        //falta: que las cedulas no se repitan, generar id de mascota automatico
        System.out.println("  ");
        System.out.println("  ");
        System.out.println("  ");
    }

}
