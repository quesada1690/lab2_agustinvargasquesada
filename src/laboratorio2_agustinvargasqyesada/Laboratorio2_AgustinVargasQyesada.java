/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorio2_agustinvargasqyesada;

import java.util.Scanner;

/**
 *
 * @author varga
 */
public class Laboratorio2_AgustinVargasQyesada {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int opcion;
        Scanner eleccion = new Scanner(System.in);
        

        do //menu con do/while para poder enciclar y que se salga hasta que el usuario quiera
        {
            System.out.println("|                       Menú principal                |");
            System.out.println("|          Digite la opcion que desee utilizar        |");
            System.out.println("|                1-Registrar persona                  |");
            System.out.println("|                2-Registrar mascota                  |");
            System.out.println("|                3-Adoptar                            |");
            System.out.println("|                4-Salir                              |");
            opcion = eleccion.nextInt();
            switch (opcion) {
                case 1:
                    clase_registrar_persona registro = new clase_registrar_persona();
                    registro.registrar_personas(args);
                    break;

                case 2:
                    clase_registrar_mascota registro_mascota = new clase_registrar_mascota();
                    registro_mascota.registrar_mascota(args);
                    break;

                case 3:
                    clase_adoptar adoptar = new clase_adoptar();
                    adoptar.adoptar_mascota(args);
                    break;

                case 4:
                    System.out.println("Que tenga un buen dia");
                    break;

                default:
                    System.out.println("Digite las opciones dadas");
                    break;

            }
        } while (opcion != 4);
    }

}
