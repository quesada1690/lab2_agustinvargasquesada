/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorio2_agustinvargasqyesada;

import java.util.ArrayList;

/**
 *
 * @author varga
 */
public class objeto_mascota {

    private int id_mascota;
    private String nombre_mascota;
    private String tipo;
    private String estado;

    public static ArrayList<objeto_mascota> listaMascotas = new ArrayList<>();

    public objeto_mascota(int id_mascota, String nombre_mascota, String tipo, String estado) {
        this.id_mascota = id_mascota;
        this.nombre_mascota = nombre_mascota;
        this.tipo = tipo;
        this.estado = estado;
    }
    
    public objeto_mascota(String estado_nuevo) {
        this.estado = estado_nuevo;
    }    

    public objeto_mascota(int id_mascota) {
        this.id_mascota = id_mascota;

    }


    public int getId_mascota() {
        return id_mascota;
    }

    public void setId_mascota(int id_mascota) {
        this.id_mascota = id_mascota;
    }

    public String getNombre_mascota() {
        return nombre_mascota;
    }

    public void setNombre_mascota(String nombre_mascota) {
        this.nombre_mascota = nombre_mascota;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
