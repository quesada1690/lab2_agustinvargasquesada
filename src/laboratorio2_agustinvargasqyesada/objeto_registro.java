/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorio2_agustinvargasqyesada;

import java.util.ArrayList;

/**
 *
 * @author varga
 */
public class objeto_registro {

    private int dueño;
    private int mascota_adoptada;

    public static ArrayList<objeto_registro> reportes = new ArrayList<>();

    public int getDueño() {
        return dueño;
    }

    public void setDueño(int dueño) {
        this.dueño = dueño;
    }

    public int getMascota_adoptada() {
        return mascota_adoptada;
    }

    public void setMascota_adoptada(int mascota_adoptada) {
        this.mascota_adoptada = mascota_adoptada;
    }

    public static ArrayList<objeto_registro> getReportes() {
        return reportes;
    }

    public static void setReportes(ArrayList<objeto_registro> reportes) {
        objeto_registro.reportes = reportes;
    }

    public objeto_registro(int dueño, int mascota_adoptada) {
        this.dueño = dueño;
        this.mascota_adoptada = mascota_adoptada;
    }



    
}
