/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorio2_agustinvargasqyesada;

import java.util.Scanner;

/**
 *
 * @author varga
 */
public class clase_registrar_persona {

    public void registrar_personas(String[] args) {

        Scanner enteros = new Scanner(System.in);
        Scanner caracter = new Scanner(System.in);

        System.out.println("  ");
        System.out.println("  ");
        System.out.println("  ");

        //registra los datos de la persona
        System.out.println("Digite la cédula:");
        int cedula = enteros.nextInt();
        String cedula_string = String.valueOf(cedula);
        String comparar = "Cedula: " + cedula_string;
        boolean buscar = objeto_persona.listaPersonas.contains(validar(cedula));
        if (buscar == validar(cedula)) {
            System.out.println("Digite el nombre:");
            String nombre = caracter.nextLine();
            System.out.println("Digite el género:");
            String genero = caracter.nextLine();
            objeto_persona.listaPersonas.add(new objeto_persona(cedula, nombre, genero));
        } else {
            System.out.println("El numero de cedula ya se registro");

        }

        for (int i = 0; i <= objeto_persona.listaPersonas.size() - 1; i++) {
            objeto_persona datos = objeto_persona.listaPersonas.get(i);
            int id = datos.getCedula();
            String nom = datos.getNombre();
            String genero_ver = datos.getGenero();
            System.out.println("Cedula: " + id + " Nombre: " + nom + " Genero: " + genero_ver);

        }
        System.out.println("  ");
        System.out.println("  ");
        System.out.println("  ");
    }

    //metodo para validar si la persona existe en el registro
    private boolean validar(int cedula) {
        boolean ced = false;
        for (int i = 0; i <= objeto_persona.listaPersonas.size() - 1; i++) {
            objeto_persona datos = objeto_persona.listaPersonas.get(i);
            int id = datos.getCedula();
            if (cedula == id){
                ced = true;
            }
            
        }
        return ced;
    }

}
