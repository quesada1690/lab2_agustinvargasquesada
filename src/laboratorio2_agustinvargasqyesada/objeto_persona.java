/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorio2_agustinvargasqyesada;

import java.util.ArrayList;

/**
 *
 * @author varga
 */
public class objeto_persona {

    private int cedula;
    private String nombre;
    private String genero;

    public static ArrayList<objeto_persona> listaPersonas = new ArrayList<>();

    public objeto_persona(int cedula, String nombre, String genero) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.genero = genero;
    }

    public int getCedula() {
        return cedula;
    }

    public void setCedula(int cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }
}
